#include "spring/Application/MonoInput8bit.h"
#include "spring/Application/BaseScene.h"
#include <qendian.h>
#include <iostream>

MonoInput8bit::MonoInput8bit(double timeSlice, unsigned sampleRate)
	:dataLength(timeSlice * sampleRate),
	channelBytes(2)
{
	audioFormat.setSampleRate(sampleRate);
	audioFormat.setChannelCount(1);
	audioFormat.setSampleSize(8);
	audioFormat.setCodec("audio/pcm");
	audioFormat.setByteOrder(QAudioFormat::BigEndian);
	audioFormat.setSampleType(QAudioFormat::UnSignedInt);

	maxAmplitude = 32767;
}


MonoInput8bit::~MonoInput8bit()
{
}

qint64 MonoInput8bit::writeData(const char *data, qint64 len)
{
	//const auto *ptr = reinterpret_cast<const unsigned char *>(data);

	std::cout << "write";
	const auto *ptr = data;

	for (auto i = 0; i < len / channelBytes; ++i) {

		qint32 value = 0;

		value = qFromBigEndian<quint8>(ptr);

		auto level = float(value) * (5. / maxAmplitude);
		timeData.push_back(level);
		ptr += channelBytes;
	}

	if (timeData.size() > dataLength)
		timeData.remove(0, timeData.size() - dataLength);

	std::cout << "data received " << len << std::endl;

	return len;
}



QAudioFormat MonoInput8bit::getAudioFormat()
{
	return audioFormat;
}

QVector<double> MonoInput8bit::vecGetData(QAudioInput *audioInput, unsigned refreshRate)
{
	QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
	if (!info.isFormatSupported(audioFormat))
		audioFormat = info.nearestFormat(audioFormat);

	audioInput = new QAudioInput(audioFormat);
	audioInput->start(this);

	return timeData;
}
void MonoInput8bit::stopAudioInput(QAudioInput *audioInput)
{
	audioInput->stop();
	audioInput = nullptr;
	delete audioInput;
}