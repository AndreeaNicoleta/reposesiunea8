#pragma once
#include <QVector>
#include <qaudioformat.h>
#include <qaudioinput.h>
#include <qiodevice.h>

class MonoInput8bit : public QIODevice
{
public:
	MonoInput8bit(double timeSlice, unsigned sampleRate);
	virtual ~MonoInput8bit();

	qint64 readData(char *data, qint64 maxlen) override
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);
		return -1;
	}

	qint64 writeData(const char *data, qint64 len) override;
	virtual void stopAudioInput(QAudioInput *audioInput);
	virtual QAudioFormat getAudioFormat();
	virtual QVector<double> vecGetData(QAudioInput *audioInput, unsigned refreshRate);

private:
	QAudioFormat audioFormat;
	QVector<double> timeData;
	const unsigned dataLength;
	const unsigned channelBytes;
	qint32 maxAmplitude;
};

